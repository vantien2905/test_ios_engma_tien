//
//  EColor.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

struct EColor {
    static let orangeColor          = UIColor(red: 55/255, green: 30/255, blue: 12/255, alpha: 1)
    static let thumTintColorOff     = UIColor(red: 111/255, green: 91/255, blue: 73/255, alpha: 1)
    static let thumTintColorOn      = UIColor(red: 223/255, green: 135/255, blue: 49/255, alpha: 1)
    static let tintColorOff         = UIColor(red: 69/255, green: 50/255, blue: 32/255, alpha: 1)
    static let tintColorOn          = UIColor(red: 93/255, green: 73/255, blue: 56/255, alpha: 1)
}
