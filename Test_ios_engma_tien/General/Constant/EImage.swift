//
//  EImage.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

struct EImage {
    static let imgTemplateOn    = UIImage(named: "template_on")!
    static let imgTemplateOff   = UIImage(named: "template_off")!
    static let imgShuffleOn     = UIImage(named: "shuffle_on")!
    static let imgShuffleOff    = UIImage(named: "shuffle_off")!
    static let imgPrevious      = UIImage(named: "previous")!
    static let imgPlay          = UIImage(named: "play")!
    static let imgPause         = UIImage(named: "pause")!
    static let imgNext          = UIImage(named: "next")!
    static let imgRepeatOn      = UIImage(named: "repeat_on")!
    static let imgRepeatOff     = UIImage(named: "repeat_off")!
    static let imgHome          = UIImage(named: "home")!
    static let imgMore          = UIImage(named: "more")!
    static let imgLogo          = UIImage(named: "logo")!
    static let imgMusic         = UIImage(named: "music")!
    static let imgMagicWand     = UIImage(named: "magic_wand")!
    static let imgThumSlider    = UIImage(named: "thum")!
    
}
