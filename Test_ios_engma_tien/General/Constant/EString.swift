//
//  EString.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/15/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

struct EString {
    static let titleSwitchMusic     = "Music Patterns"
    static let titleSwitchEffect    = "Effects Pattern"
    static let sliderSpeed          = "Speed"
    static let sliderVolume         = "Volume"
    static let sliderBrightness     = "Brightness"
}
