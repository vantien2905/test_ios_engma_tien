//
//  UIViewController+Extension.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

extension UIViewController {
    static func initFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>(_ : T.Type) -> T {
            return T(nibName: String(describing: T.self), bundle: nil)
        }
        
        return instantiateFromNib(self)
    }
}

