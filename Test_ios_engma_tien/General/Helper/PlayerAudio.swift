//
//  PlayerAudio.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/16/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import AVFoundation

class PlayerAudio {
    var player: AVPlayer?
    
    var playerItem: AVPlayerItem?
    
    init(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
    }
}
