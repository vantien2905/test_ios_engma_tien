//
//  SwitchView.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit
import RxSwift

class SwitchView: BaseViewXib {
    
    @IBOutlet weak var lbTitle  : UILabel!
    @IBOutlet weak var swChoose : UISwitch!
    
    var isCheck = Variable<Bool>(false)
    
    override func setUpView() {
        swChoose.addTarget(self, action: #selector(switchStateDidChange), for: .valueChanged)
        swChoose.setOn(true, animated: false)
        swChoose.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        swChoose.onTintColor = EColor.tintColorOn
        swChoose.tintColor = EColor.tintColorOn
    }
    
    @objc func switchStateDidChange(_ sender: UISwitch){
        isCheck.value = sender.isOn
        if (sender.isOn == true){
            swChoose.thumbTintColor = EColor.thumTintColorOn
        }
        else{
            swChoose.thumbTintColor = EColor.thumTintColorOff
        }
    }
    
    func setSwitch(title: String, setON: Bool) {
        self.lbTitle.text = title
        swChoose.setOn(setON, animated: true)
        swChoose.thumbTintColor = setON ? EColor.thumTintColorOn : EColor.thumTintColorOff
    }
}
