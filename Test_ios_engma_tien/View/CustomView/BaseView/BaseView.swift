//
//  BaseView.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

class BaseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpViews()
    }
    
    func setUpViews() {
        
    }
}
