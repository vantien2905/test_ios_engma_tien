//
//  SliderView.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit

class SliderView: BaseViewXib {
    
    @IBOutlet weak var slider   : UISlider!
    @IBOutlet weak var lbValue  : UILabel!
    @IBOutlet weak var lbTitle  : UILabel!
    
    override func setUpView() {
        slider.setThumbImage(EImage.imgThumSlider.resizeImage(targetSize: CGSize(width: 20, height: 20)), for: .normal)
    }
    
    @IBAction func slider(_ sender: Any) {
        lbValue.text = String(Int(slider.value))
    }
}
