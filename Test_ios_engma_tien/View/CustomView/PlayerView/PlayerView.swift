//
//  PlayerView.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//
import AVFoundation
import UIKit
import RxSwift

class PlayerView: BaseViewXib {
    
    @IBOutlet weak var sliderTop        : SliderView!
    @IBOutlet weak var sliderBottom     : SliderView!
    @IBOutlet weak var btnShuffle       : UIButton!
    @IBOutlet weak var btnPrevious      : UIButton!
    @IBOutlet weak var btnPlay          : UIButton!
    @IBOutlet weak var btnNext          : UIButton!
    @IBOutlet weak var btnRepeat        : UIButton!
    @IBOutlet weak var imgMusic         : UIImageView!
    
    var isShuffle = false
    var isPlay = Variable<Bool>(false)
    var isRepeat = false
    var isMusic = false {
        didSet {
            if isMusic {
                playAudio = PlayerAudio(urlString: linkMp3[0])
            } else {
                playAudio = PlayerAudio(urlString: linkMp3[1])
            }
        }
    }
    
    var playAudio: PlayerAudio?
    
    var player:AVPlayer?
    
    var linkMp3 = ["https://f.x2convert.com/files/2018/9/1/lien_khuc_nhac_mien_tay_nhac_dan_ca_que_huong_mien_tay_hay_nhat_2017.mp3",
                   "http://rfcmedia.streamguys1.com/Newport.mp3"]
    
    @IBAction func btnShuffleTapped() {
        isShuffle = !isShuffle
        isShuffle ? btnShuffle.setImage(EImage.imgShuffleOn, for: .normal)
                  : btnShuffle.setImage(EImage.imgShuffleOff, for: .normal)
    }
    
    @IBAction func btnPlayTapped() {
        isPlay.value = !isPlay.value
    }
    
    @IBAction func btnRepeatTapped() {
        isRepeat = !isRepeat
        isRepeat ? btnRepeat.setImage(EImage.imgRepeatOn, for: .normal)
                 : btnRepeat.setImage(EImage.imgRepeatOff, for: .normal)
    }
    
    func setData(topTitle: String, bottomTitle: String, image: UIImage) {
        sliderTop.lbTitle.text = topTitle
        sliderBottom.lbTitle.text = bottomTitle
        imgMusic.image = image
    }
}
