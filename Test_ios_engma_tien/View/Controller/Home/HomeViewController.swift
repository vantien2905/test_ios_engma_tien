//
//  HomeViewController.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit
import AVFoundation

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var sMusic       : SwitchView!
    @IBOutlet weak var sEffect      : SwitchView!
    @IBOutlet weak var playerMusic  : PlayerView!
    @IBOutlet weak var playerEffect : PlayerView!
    @IBOutlet weak var imgContent   : UIImageView!
    @IBOutlet weak var btnOnOff     : UIButton!
    @IBOutlet weak var hightMusic   : NSLayoutConstraint!
    @IBOutlet weak var hightEffect  : NSLayoutConstraint!
    
    //check button on/off
    var isOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindData()
        setUpPlayBackground()
    }
    
    //-- Play audio background
    func setUpPlayBackground() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            print(error)
        }
    }
    
    override func setUpViews() {
        playerMusic.isMusic = true
        playerEffect.isMusic = false
        sMusic.setSwitch(title: EString.titleSwitchMusic, setON: true)
        sEffect.setSwitch(title: EString.titleSwitchEffect, setON: false)
        playerMusic.setData(topTitle    : EString.sliderVolume,
                            bottomTitle : EString.sliderBrightness,
                            image       : EImage.imgMusic)
        playerEffect.setData(topTitle   : EString.sliderSpeed,
                             bottomTitle: EString.sliderBrightness,
                             image      : EImage.imgMagicWand)
        sMusic.setBorder(borderWidth: 0.5, borderColor: EColor.orangeColor, cornerRadius: 0)
        btnOnOff.setBorder(cornerRadius: btnOnOff.bounds.height/2)
        //-- Hide view effect no have animation
        self.hightMusic.constant = AppConstant.heightPlayerView
        self.hightEffect.constant = 0
        self.playerEffect.alpha = 0
    }
    
    override func setUpNavigation() {
        addButtonImageToNavigation(image: EImage.imgHome, style: .left, action: nil)
        addButtonImageToNavigation(image: EImage.imgMore, style: .right, action: nil)
        navigationController?.navigationBar.barTintColor = .black
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        imageView.contentMode = .scaleAspectFit
        imageView.image = EImage.imgLogo
        self.navigationItem.titleView = imageView
    }
    
    @IBAction func btnOnOffTapped() {
        isOn = !isOn
        if isOn {
            btnOnOff.setTitle("ON", for: .normal)
            btnOnOff.setBorder(borderWidth: 1, borderColor: .white, cornerRadius: btnOnOff.bounds.height/2)
            imgContent.image = EImage.imgTemplateOn
        } else {
            btnOnOff.setTitle("OFF", for: .normal)
            btnOnOff.setBorder(borderWidth: 1, borderColor: EColor.orangeColor, cornerRadius: btnOnOff.bounds.height/2)
            imgContent.image = EImage.imgTemplateOff
        }
    }
    
    //-- Hide effect view and show music view
    func showMusic() {
        UIView.animate(withDuration: 0.5) {
            self.hightMusic.constant = AppConstant.heightPlayerView
            self.playerMusic.alpha = 1
            self.hightEffect.constant = 0
            self.playerEffect.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    //-- Show effect view and hide music view
    func hideMusic() {
        UIView.animate(withDuration: 0.5) {
            self.hightMusic.constant = 0
            self.playerMusic.alpha = 0
            self.hightEffect.constant = AppConstant.heightPlayerView
            self.playerEffect.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
    
    func bindData() {
        sMusic.isCheck.asObservable().skip(1).subscribe(onNext: { [weak self](value) in
            guard let `self` = self else { return }
            if value {
                self.sEffect.swChoose.setOn(false, animated: true)
                self.sEffect.swChoose.thumbTintColor = EColor.thumTintColorOff
                self.showMusic()
            } else {
                self.sEffect.swChoose.setOn(true, animated: true)
                self.sEffect.swChoose.thumbTintColor = EColor.thumTintColorOn
                self.hideMusic()
            }
        }).disposed(by: disposeBag)
        
        sEffect.isCheck.asObservable().skip(1).subscribe(onNext: { [weak self](value) in
            guard let `self` = self else { return }
            if value {
                self.sMusic.swChoose.setOn(false, animated: true)
                self.sMusic.swChoose.thumbTintColor = EColor.thumTintColorOff
                self.hideMusic()
            } else {
                self.sMusic.swChoose.setOn(true, animated: true)
                self.sMusic.swChoose.thumbTintColor = EColor.thumTintColorOn
                self.showMusic()
            }
        }).disposed(by: disposeBag)
        
        playerMusic.isPlay.asObservable().subscribe(onNext: { [weak self](value) in
            guard let `self` = self else { return }
            if value {
                self.playerEffect.isPlay.value = false
                self.playerMusic.playAudio?.player?.play()
                self.playerMusic.btnPlay.setImage(EImage.imgPlay, for: .normal)
            } else {
                self.playerMusic.playAudio?.player?.pause()
                self.playerMusic.btnPlay.setImage(EImage.imgPause, for: .normal)
            }
        }).disposed(by: disposeBag)
        
        playerEffect.isPlay.asObservable().subscribe(onNext: {[weak self] (value) in
            guard let `self` = self else { return }
            if value {
                self.playerMusic.isPlay.value = false
                self.playerEffect.playAudio?.player?.play()
                self.playerEffect.btnPlay.setImage(EImage.imgPlay, for: .normal)
            } else {
                self.playerEffect.playAudio?.player?.pause()
                self.playerEffect.btnPlay.setImage(EImage.imgPause, for: .normal)
            }
        }).disposed(by: disposeBag)
    }
}
