//
//  BaseViewController.swift
//  Test_ios_engma_tien
//
//  Created by Tien Dinh on 9/14/18.
//  Copyright © 2018 Tien Dinh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum StyleNavigation {
    case left
    case right
}

class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        setUpViews()
    }
    
    func setUpViews() {
    }
    
    func setUpNavigation() {
    
    }
    
    //add button image for navigation
    func addButtonImageToNavigation(image: UIImage, style: StyleNavigation, action: Selector?) {
        let btn = UIButton()
        btn.setImage(image, for: .normal)
        if let _action = action {
            btn.addTarget(self, action: _action, for: .touchUpInside)
        }
        btn.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        btn.imageView?.contentMode = .scaleAspectFit
        
        let button = UIBarButtonItem(customView: btn)
        let currWidth = button.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = button.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        if style == .left {
            btn.contentHorizontalAlignment = .left
            self.navigationItem.leftBarButtonItem = button
        } else {
            self.navigationItem.rightBarButtonItem = button
            btn.contentHorizontalAlignment = .right
        }
    }
}
